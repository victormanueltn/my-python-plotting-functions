"""
Created on 11th July 2017
@author: Victor Manuel Trejo Navas
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm

def set_single_plot(X,Y, Xlabel = 'X', Ylabel = 'Y',linestyle = 'solid', linewidth = 3, marker = '', markersize = 10, color = 'k', fontsize = 18, label = ''):
    """ Simple wrapper function for plt.plot. Adds a plot to the current figure with default parameters meant to provide a decent plot. Several data sets can be  added to same plot if called multiple times. 

    :param X: independent variable of the plot.
    :param Y: dependent variable of the plot.

    """
    plt.plot(X,Y, marker=marker, markersize=markersize, markeredgecolor = color, linestyle = linestyle, linewidth=linewidth, color=color, label=label) 
    plt.xlabel(Xlabel, fontsize = fontsize)
    plt.ylabel(Ylabel, fontsize = fontsize)
    plt.legend(loc='best',prop={'size':fontsize})

def single_plot_savefig(X,Y,path,fileName,extension = '.png', dpi = 500, Xlabel = 'X', Ylabel = 'Y',linestyle = 'solid', linewidth = 3, marker = '', markersize = 10, color = 'k'):
    """ Calls set_single_plot and then saves the figure.

    :param X: independent variable of the plot.
    :param Y: dependent variable of the plot.
    :param string path: path where the figure will be saved to. Has to finish with a backslash.
    :param string fileName: Name given to the saved file.
    :param string extension: File extension. Examples are '.png' and '.pdf'
    :returns: a file containing the plot.
    """
    plt.figure()
    set_single_plot(X,Y, Xlabel = Xlabel, Ylabel = Ylabel, linestyle = linestyle, linewidth = linewidth, marker = marker, markersize = markersize, color = color) 
    plt.savefig(path+fileName+extension,dpi=dpi)


def set_two_dimensional_surface_plot(X, Y, Z, Xlabel = 'X', Ylabel = 'Y', Zlabel = 'Z', fontsize = 18, fontweight = 'bold', nbPoints = 500, nbContours = 15, linewidths = 2, colorMap = plt.cm.RdBu_r):
	""" Creates a surface plot for Z = f(X,Y) 

	:param X: first independent variable of the plot.
	:param Y: second independent variable of the plot.
	:param Z: dependent variable of the plot.

	"""
	
	X = np.array(X)
	Y = np.array(Y)
	
	Xi = np.linspace(X.min(),X.max(),nbPoints)
	Yi = np.linspace(Y.min(),Y.max(),nbPoints)

	Zi = griddata((X, Y), Z, (Xi[None,:], Yi[:,None]), method='cubic')

	CS = plt.contourf(Xi,Yi,Zi,nbContours,cmap=colorMap)
	colorBar = plt.colorbar()

	colorBar.ax.set_title(Zlabel, fontsize = fontsize*0.8, fontweight = fontweight, y = 1.02)

	plt.xlim(X.min(),X.max())
	plt.ylim(Y.min(),Y.max())
	plt.ylabel(Ylabel, fontsize = fontsize, fontweight = fontweight)
	plt.xlabel(Xlabel, fontsize = fontsize, fontweight = fontweight)
	
	
def set_two_dimensional_surface_plot_with_numbered_contours(X, Y, Z, Xlabel = 'X', Ylabel = 'Y', Zlabel = 'Z', fontsize = 18, fontweight = 'bold', nbPoints = 500, nbContours = 15, linewidths = 2, firstColorMap = plt.cm.RdBu_r):
	""" Creates a surface plot for Z = f(X,Y) with contours showing the values of Z

	:param X: first independent variable of the plot.
	:param Y: second independent variable of the plot.
	:param Z: dependent variable of the plot.

	"""
	X = np.array(X)
	Y = np.array(Y)

	Xi = np.linspace(X.min(),X.max(),nbPoints)
	Yi = np.linspace(Y.min(),Y.max(),nbPoints)

	Zi = griddata((X, Y), Z, (Xi[None,:], Yi[:,None]), method='cubic')

	CS = plt.contour(Xi,Yi,Zi,nbContours,linewidths=linewidths,cmap=plt.cm.Set2_r)
	plt.clabel(CS,inline=True,fmt='%1.2f',fontsize=fontsize)

	CS = plt.contourf(Xi,Yi,Zi,nbContours,cmap=firstColorMap)
	colorBar = plt.colorbar()

	colorBar.ax.set_title(Zlabel, fontsize = fontsize*0.8, fontweight = fontweight, y = 1.02)

	plt.xlim(X.min(),X.max())
	plt.ylim(Y.min(),Y.max())
	plt.ylabel(Ylabel, fontsize = fontsize, fontweight = fontweight)
	plt.xlabel(Xlabel, fontsize = fontsize, fontweight = fontweight)
	
	
def set_three_dimensional_surface_plot(X, Y, Z, Xlabel = 'X', Ylabel = 'Y', Zlabel = 'Z', fontsize = 18, fontweight = 'bold', nbPoints = 500, nbContours = 15, linewidths = 2, colorMap = plt.cm.RdBu_r):
	""" Creates a three-dimensional surface plot for Z = f(X,Y)

	:param X: first independent variable of the plot.
	:param Y: second independent variable of the plot.
	:param Z: dependent variable of the plot.

	"""
	Xi = np.linspace(X.min(),X.max(),nbPoints)
	Yi = np.linspace(Y.min(),Y.max(),nbPoints)
	Zi = griddata((X, Y), Z, (Xi[None,:], Yi[:,None]), method='cubic')
	fig = plt.figure()
	ax = fig.gca(projection='3d')
	X, Y, Z = Xi, Yi, Zi
	X, Y = np.meshgrid(X,Y)
	Z_colorMap = np.array(Z)
	Z_colorMap[np.isnan(Z_colorMap)] = 0
	Z_colorMapNormalized = Z_colorMap/Z_colorMap.max()
	fontsize = 0.8*fontsize
 	surf = ax.plot_surface(X, Y, Z, rstride=8, cstride=8, alpha=1.0, facecolors=plt.cm.RdBu_r(Z_colorMapNormalized),vmin = np.amin(Z), vmax =np.amax(Z)) 
	plt.ylabel(Ylabel, fontsize = fontsize, fontweight = fontweight)
	plt.xlabel(Xlabel, fontsize = fontsize, fontweight = fontweight)
	ax.set_zlabel(Zlabel, fontsize = fontsize, fontweight = fontweight)
	for t in ax.zaxis.get_major_ticks(): t.label.set_fontsize(fontsize*0.8)
	for t in ax.xaxis.get_major_ticks(): t.label.set_fontsize(fontsize*0.8)
	for t in ax.yaxis.get_major_ticks(): t.label.set_fontsize(fontsize*0.8)
	m = cm.ScalarMappable(cmap = colorMap)
	m.set_array(Z_colorMap)
	plt.colorbar(m)

def set_three_dimensional_surface_plot_produceMultipleViews(X, Y, Z, Xlabel = 'X', Ylabel = 'Y', Zlabel = 'Z', fontsize = 18, fontweight = 'bold', nbPoints = 500, nbContours = 15, linewidths = 2, colorMap = plt.cm.RdBu_r):
	""" Creates a three-dimensional surface plot for Z = f(X,Y) and stores 360 rotations of the plot in images

	:param X: first independent variable of the plot.
	:param Y: second independent variable of the plot.
	:param Z: dependent variable of the plot.

	"""
	Xi = np.linspace(X.min(),X.max(),nbPoints)
	Yi = np.linspace(Y.min(),Y.max(),nbPoints)
	Zi = griddata((X, Y), Z, (Xi[None,:], Yi[:,None]), method='cubic')
	fig = plt.figure()
	ax = fig.gca(projection='3d')
	X, Y, Z = Xi, Yi, Zi
	X, Y = np.meshgrid(X,Y)
	Z_colorMap = np.array(Z)
	Z_colorMap[np.isnan(Z_colorMap)] = 0
	Z_colorMapNormalized = Z_colorMap/Z_colorMap.max()
	fontsize = 0.8*fontsize
 	surf = ax.plot_surface(X, Y, Z, rstride=8, cstride=8, alpha=1.0, facecolors=plt.cm.RdBu_r(Z_colorMapNormalized),vmin = np.amin(Z), vmax =np.amax(Z)) 
	plt.ylabel(Ylabel, fontsize = fontsize, fontweight = fontweight)
	plt.xlabel(Xlabel, fontsize = fontsize, fontweight = fontweight)
	ax.set_zlabel(Zlabel, fontsize = fontsize, fontweight = fontweight)
	for t in ax.zaxis.get_major_ticks(): t.label.set_fontsize(fontsize*0.8)
	for t in ax.xaxis.get_major_ticks(): t.label.set_fontsize(fontsize*0.8)
	for t in ax.yaxis.get_major_ticks(): t.label.set_fontsize(fontsize*0.8)
	m = cm.ScalarMappable(cmap = colorMap)
	m.set_array(Z_colorMap)
	plt.colorbar(m)
	for ii in xrange(0,360,1):
        	ax.view_init(elev=40., azim=ii)
	        plt.savefig("3Dviews%d.png" % ii, dpi = 500)
	


def set_histogram(Y, bins = 'auto', Xlabel = 'X', Ylabel = 'Y', label = None, title = None, alpha = 0.7, color = None, edgecolor = None, linewidth = 1):
	""" Creates a histogram 

	:param Y: frequency values of the histogram.

	"""

        plt.hist(a, bins=bins, label = label, alpha = alpha, color = color, linewidth = linewidth, edgecolor = edgecolor)
        plt.title(title)
        plt.xlabel(Xlabel, fontsize = fontsize)
        plt.ylabel(Ylabel, fontsize = fontsize)
        plt.legend(loc = 'best', prop={'size':fontsize})
