.. myPlottingFunctions documentation master file, created by
   sphinx-quickstart on Tue Jul 11 13:49:31 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to myPlottingFunctions's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: myPlottingFunctions
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
